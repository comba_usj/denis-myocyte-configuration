//
//  model_constant.h
//  Create_Config_DENIS_Natrium
//
//  Created by Jesús Carro Fernández on 24/6/16.
//  Copyright © 2016 Jesús Carro Fernández. All rights reserved.
//

#ifndef model_constant_h
#define model_constant_h

#include <string>
#include "modification_type.h"
#include "model_element.h"

using namespace std;
class ModelConstant: public ModelElement{

public:
    double value;
    ModificationType type;

    ModelConstant():ModelElement(){
        value=0;
        type = RANDOM_ABSOLUTE;
    }

    ModelConstant(string name, string component, double value, ModificationType type):
        ModelElement(name, component){
        this->value = value;
        this->type=type;
    }

};
#endif /*model_constant_h*/

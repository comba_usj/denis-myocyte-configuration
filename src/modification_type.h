#pragma once

enum ModificationType { RANDOM_ABSOLUTE, RANDOM_LOGARITMIC, GRADIENT_ABSOLUTE, GRADIENT_LOGARITMIC };

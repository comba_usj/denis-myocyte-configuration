//
//  model_state_variable.h
//  Create_Config_DENIS_Natrium
//
//  Created by Jesús Carro Fernández on 24/6/16.
//  Copyright © 2016 Jesús Carro Fernández. All rights reserved.
//

#ifndef model_element
#define model_element

#include <string>

using namespace std;

class ModelElement{

public:
    string name;
    string component;


    ModelElement(){
        name="";
        component="";
    }

    ModelElement(string name, string component){
        this->name = string(name);
        this->component = string(component);
    }

};

#endif // model_element

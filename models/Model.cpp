////
// Created by Jesús Carro Fernández on 2/1/20.
//
#include "Model.h"
#include <string.h>

using namespace std;

Model::~Model() {
    delete[] constantsNames;
    delete[] statesNames;
    delete[] ratesNames;
    delete[] algebraicNames;
    delete[] constants;
    delete[] initStates;
}

void Model::initVectors() {
    constantsNames = new const char *[constantsLength];
    statesNames = new const char *[statesLength];
    ratesNames = new const char *[statesLength];
    algebraicNames = new const char *[algebraicsLength];

    setNames();

    constants = new double[constantsLength];
    initStates = new double[statesLength];

    initConstsAndStates();
    setDependentConsts();
}


int Model::getNameId(const char *variable, const char *component, const char **names, int namesLength) {
    int index = -1;
    int counter = 0;
    string completName(variable);
    completName += " in component ";
    completName += component;
    for (int i = 0; i < namesLength; i++) {
        if (strncmp(completName.c_str(), names[i], completName.length()) == 0) {
            index = i;
            counter++;
        }
    }

    switch (counter) {
        case 1:
            return index;
        case 0:
            return -1;
        default:
            return -counter;
    }
}

int Model::getConstantNameId(const char *variable, const char *component) {
    return getNameId(variable, component, constantsNames, constantsLength);
}

int Model::getAlgebraicNameId(const char *variable, const char *component) {
    return getNameId(variable, component, algebraicNames, algebraicsLength);
}

int Model::getStatesNameId(const char *variable, const char *component) {
    return getNameId(variable, component, statesNames, statesLength);
}

int Model::getRatesNameId(const char *variable, const char *component) {
    return getNameId(variable, component, ratesNames, statesLength);
}

/*std::vector<ConstantToChange> Model::modifyConstants(std::vector<ConstantToChange> constantsToModify){

    std::vector<ConstantToChange> constantsNotModified;

    for(int i=0;i<constantsToModify.size();i++){
        int id = getConstantNameId(constantsToModify[i].getName(),
                constantsToModify[i].getComponent());
        if(id<0)
            constantsNotModified.push_back(constantsToModify[i]);
        else
            constantsToChange[id] = constantsToModify[i].getValue();
    }

    setDependentConsts();

    return constantsNotModified;
}*/
//
//  model_modif_constant.h
//  Create_Config_DENIS_Natrium
//
//  Created by Jesús Carro Fernández on 24/6/16.
//  Copyright © 2016 Jesús Carro Fernández. All rights reserved.
//

#ifndef model_modif_constant_h
#define model_modif_constant_h

#include <string>
#include <math.h>
#include "modification_type.h"

using namespace std;

class ModelModifConstant{

public:
    string name;
    string component;
    double initValue;
    double endValue;
	ModificationType type;


    ModelModifConstant(){
        name="";
        component="";
        initValue=0;
        endValue=0;
        type = RANDOM_ABSOLUTE;
    }

    ModelModifConstant(string name, string component, double initValue, double endValue, ModificationType type){
        this->name = string(name);
        this->component = string(component);
        this->initValue = initValue;
        this->endValue = endValue;
        this->type = type;
    }


    ModelModifConstant(string name, string component, double initValue, ModificationType type){
        this->name = string(name);
        this->component = string(component);
        this->initValue = initValue;
        this->endValue = initValue;
        this->type = type;
    }

    double getValue(bool end=false)
    {
        double diff = endValue-initValue;
		double value = -1;

		switch (type)
		{
		case RANDOM_ABSOLUTE:
			value = initValue + (rand() % 100000)*diff / 100000.0;
			break;
		case RANDOM_LOGARITMIC:
			value = pow(10, (initValue + (rand() % 100000)*diff / 100000.0) / 20);
			break;
		case GRADIENT_ABSOLUTE:
			if (end)
				value = endValue;
			else
				value = initValue;
			break;
		case GRADIENT_LOGARITMIC:
			if (end)
				value = pow(10, endValue / 20);
			else
				value = pow(10, initValue / 20);
			break;
		default:
			break;
		}

        return value;
    }
};


#endif /* model_modif_constant_h */

// CreateConfigurationNatrium.cpp: define el punto de entrada de la aplicación de consola.
//

#include <cstdio>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "model_constant.h"
#include "model_element.h"
#include "config.h"
#include "global_config.h"
#include "models.h"
#include "condor_functions.h"

using namespace std;

void createDENISNatriumConfigFile(Config config);

void printHeader();

void createMainFileAndConfigurationStructures(GlobalConfig globalConfig);

void createCondorFiles(GlobalConfig globalConfig);

void createCondorScript(GlobalConfig globalConfig);

int main(int argc, char **argv) {

    printHeader();

    if (argc < 1) {
        cout << "ERROR: There is not configuration file" << endl;
        cout << "       You MUST indicate a configuration file when you call this program" << endl;
        cout << "       " << argv[0] << " <Global_Configuration_file> " << endl;
        return -1;
    }

    char *globalConfigFileName = argv[1];
    GlobalConfig globalConfig(globalConfigFileName);
    createMainFileAndConfigurationStructures(globalConfig);
    createCondorFiles(globalConfig);
    createCondorScript(globalConfig);

    return 0;
}


void printHeader() {
    cout << endl << "           ==== DENIS Batch Configuration ===" << endl << endl;
    cout << "                                              Jesus Carro Fernandez" << endl;
    cout << "                                                      jcarro@usj.es" << endl << endl;
    cout << "                                                      DENIS Project" << endl;
    cout << "                                                       denis.usj.es" << endl << endl;
    cout << "                                              Universidad San Jorge" << endl;
    cout << "                                                         www.usj.es" << endl;
}

void createMainFileAndConfigurationStructures(GlobalConfig globalConfig) {
    stringstream fileConfigName;
    fileConfigName << globalConfig.path << "/" << globalConfig.prefix << ".csv";
    cout << "File config name: " << fileConfigName.str().c_str() << endl;

    stringstream directoryName;
    directoryName << "mkdir " << globalConfig.path << "/" << globalConfig.prefix;
    cout << "Directory: " << directoryName.str() << endl;
    cout << system(directoryName.str().c_str()) << endl;

    for (int protocolId = 0; protocolId < globalConfig.numProtocols; protocolId++) {
        stringstream subdirectoryName;
        subdirectoryName << "mkdir -m 755 " << globalConfig.path << "/" << globalConfig.prefix << "/"
                         << globalConfig.protocolNames[protocolId];
        system(subdirectoryName.str().c_str());
        subdirectoryName << "/Out";
        system(subdirectoryName.str().c_str());

    }

    ofstream fileConfigOfstream;
    fileConfigOfstream.open(fileConfigName.str().c_str(), ios::out);
    fileConfigOfstream << "\"simId\"";

    for (int i = 0; i < globalConfig.numGlobalConstantsToChange; i++) {
        fileConfigOfstream << ";\"" << globalConfig.globalConstantsToChange[i].name << " ("
                           << globalConfig.globalConstantsToChange[i].component << ")\"";
    }

    fileConfigOfstream << endl;

    cout << "Creating configuration structures..." << endl;

    Config *configurations = new Config[globalConfig.numProtocols
                                        * globalConfig.numCombinations];
    for (int i = 0; i < globalConfig.numCombinations; i++) {

        fileConfigOfstream << i;
        double *constValues = new double[globalConfig.numGlobalConstantsToChange];
        for (int k = 0; k < globalConfig.numGlobalConstantsToChange; k++) {
            constValues[k] = globalConfig.globalConstantsToChange[k].getValue(k == i);
            fileConfigOfstream << ";" << constValues[k];
        }
        fileConfigOfstream << endl;

        for (int j = 0; j < globalConfig.numProtocols; j++) {
            stringstream ss;
            ss << globalConfig.path << "/" << globalConfig.prefix << "/" << globalConfig.protocolNames[j] << "/"
               << globalConfig.prefix << "-"
               << globalConfig.protocolNames[j] << "-conf_" << i << ".xml";

            int numConstantsToChange = globalConfig.numGlobalConstantsToChange
                                       + globalConfig.numProtocolConstantsToChange[j];
            int numStatesToSave = globalConfig.numStatesToSave
                                  + globalConfig.numProtocolStatesToSave[j];
            int numRatesToSave = globalConfig.numRatesToSave
                                 + globalConfig.numProtocolRatesToSave[j];
            int numAlgebraicsToSave = globalConfig.numAlgebraicsToSave
                                      + globalConfig.numProtocolAlgebraicsToSave[j];

            ModelConstant *constantsToChange = new ModelConstant[numConstantsToChange];

            for (int k = 0; k < globalConfig.numGlobalConstantsToChange; k++) {
                constantsToChange[k] = ModelConstant(
                        globalConfig.globalConstantsToChange[k].name,
                        globalConfig.globalConstantsToChange[k].component,
                        constValues[k],
                        globalConfig.globalConstantsToChange[k].type);

            }

            int ind = globalConfig.numGlobalConstantsToChange;

            for (int k = 0; k < globalConfig.numProtocolConstantsToChange[j]; k++) {
                bool previouslyAdded = false;
                ModelConstant constantInProtocol = ModelConstant(
                        globalConfig.protocolConstantsToChange[j][k].name,
                        globalConfig.protocolConstantsToChange[j][k].component,
                        globalConfig.protocolConstantsToChange[j][k].getValue(),
                        globalConfig.protocolConstantsToChange[j][k].type);

                for (int l = 0; l < globalConfig.numGlobalConstantsToChange; l++) {
                    if (strcmp(globalConfig.globalConstantsToChange[l].name.c_str(),
                               globalConfig.protocolConstantsToChange[j][k].name.c_str()) == 0 &&
                        strcmp(globalConfig.globalConstantsToChange[l].component.c_str(),
                               globalConfig.protocolConstantsToChange[j][k].component.c_str()) == 0) {
                        if (constantInProtocol.type == GRADIENT_ABSOLUTE) {
                            constantsToChange[l].value *= constantInProtocol.value;
                        } else {
                            constantsToChange[l] = constantInProtocol;
                        }
                        numConstantsToChange--;
                        previouslyAdded = true;
                        break;
                    }

                }
                if (!previouslyAdded) {
                    constantsToChange[ind] = constantInProtocol;
                    ind++;
                }
            }

            ModelElement *statesToSave =
                    new ModelElement[numStatesToSave];
            for (int k = 0; k < globalConfig.numStatesToSave; k++) {
                statesToSave[k] = ModelElement(
                        globalConfig.statesToSave[k].name,
                        globalConfig.statesToSave[k].component);
            }

            for (int k = globalConfig.numStatesToSave; k < numStatesToSave;
                 k++) {
                int ind = k - globalConfig.numStatesToSave;
                statesToSave[k] = ModelElement(
                        globalConfig.protocolStatesToSave[j][ind].name,
                        globalConfig.protocolStatesToSave[j][ind].component);
            }


            ModelElement *ratesToSave =
                    new ModelElement[numRatesToSave];
            for (int k = 0; k < globalConfig.numRatesToSave; k++) {
                ratesToSave[k] = ModelElement(
                        globalConfig.ratesToSave[k].name,
                        globalConfig.ratesToSave[k].component);
            }

            for (int k = globalConfig.numRatesToSave; k < numRatesToSave;
                 k++) {
                int ind = k - globalConfig.numRatesToSave;
                ratesToSave[k] = ModelElement(
                        globalConfig.protocolRatesToSave[j][ind].name,
                        globalConfig.protocolRatesToSave[j][ind].component);
            }

            ModelElement *algebraicsToSave =
                    new ModelElement[numAlgebraicsToSave];
            for (int k = 0; k < globalConfig.numAlgebraicsToSave; k++) {
                algebraicsToSave[k] = ModelElement(
                        globalConfig.algebraicsToSave[k].name,
                        globalConfig.algebraicsToSave[k].component);
            }

            for (int k = globalConfig.numAlgebraicsToSave; k < numAlgebraicsToSave; k++) {
                int ind = k - globalConfig.numAlgebraicsToSave;
                algebraicsToSave[k] = ModelElement(
                        globalConfig.protocolAlgebraicsToSave[j][ind].name,
                        globalConfig.protocolAlgebraicsToSave[j][ind].component);
            }

            configurations[i * globalConfig.numProtocols + j] =
                    Config(ss.str(),
                           globalConfig.modelNames[j], globalConfig.time[j],
                           globalConfig.solver[j], globalConfig.dt[j],
                           globalConfig.freqOut[j], globalConfig.startOut[j],
                           globalConfig.startPost[j], globalConfig.vNames[j],
                           globalConfig.vComponents[j],
                           constantsToChange, numConstantsToChange,
                           statesToSave, numStatesToSave,
                           ratesToSave, numRatesToSave,
                           algebraicsToSave, numAlgebraicsToSave);
        }

    }

    fileConfigOfstream.close();
    cout << "Saving configuration structures..." << endl;

    for (int i = 0; i < globalConfig.numProtocols * globalConfig.numCombinations; i++)
        createDENISNatriumConfigFile(configurations[i]);

    cout << "Configurations saved." << endl;
}


void createDENISNatriumConfigFile(Config config) {

    ofstream outfile(config.fileName.c_str());

    outfile.precision(15);

    Model *model = getModelByName(config.modelName.c_str());
    double *constantsValues = model->getConstants();
    double *statesValues = model->getInitStates();

    const char **constants = model->getConstantNames();
    const char **rates = model->getRateNames();
    const char **states = model->getStateNames();
    const char **algebraic = model->getAlgebraicNames();

    outfile << "<?xml version=\"1.0\"?>" << endl;
    outfile << "<simulation>" << endl;
    outfile << "    <model>" << config.modelName << "</model>" << endl;
    outfile << "    <time>" << config.time << "</time>" << endl;
    outfile << "    <dt>" << config.dt << "</dt>" << endl;
    outfile << "    <solver>" << config.solver << "</solver>" << endl;
    outfile << "    <postprocessing>" << endl;
    outfile << "        <start>" << config.startPost << "</start>" << endl;
    outfile << "        <potential>" << endl;
    outfile << "            <name>" << config.vName << "</name>" << endl;
    outfile << "            <component>" << config.vComponent << "</component>" << endl;
    outfile << "        </potential>" << endl;
    outfile << "    </postprocessing>" << endl;

    if (config.numConstantsToChange > 0)
        outfile << "    <constants_to_change>" << endl;

    for (int i = 0; i < config.numConstantsToChange; i++) {

        double value = config.constantsToChange[i].value;

        int constantId = model->getConstantNameId(config.constantsToChange[i].name.c_str(),
                                                  config.constantsToChange[i].component.c_str());

        if (constantId < 0) {
            cout << endl << "---------------------------------------------------------------" << endl;
            cout << endl << "      ERROR: Constant not found: " << config.constantsToChange[i].name
                 << " (" << config.constantsToChange[i].component << ")" << endl;
            cout << endl << "      Configuration file: " << config.fileName << endl;
            cout << endl << "---------------------------------------------------------------" << endl << endl;
            exit(-1);
        }

        if (config.constantsToChange[i].type) {
            value *= constantsValues[constantId];
        }

        outfile << "        <constant>" << endl;
        outfile << "            <name>" << config.constantsToChange[i].name << "</name>"
                << endl;
        outfile << "            <component>" << config.constantsToChange[i].component
                << "</component>" << endl;
        outfile << "            <value>" << value << "</value>" << endl;
        outfile << "        </constant>" << endl;
    }

    if (config.numConstantsToChange > 0)
        outfile << "    </constants_to_change>" << endl;

    outfile << "    <output>" << endl;
    outfile << "        <start>" << config.startOut << "</start>" << endl;
    outfile << "        <freq>" << config.freqOut << "</freq>" << endl;

    if (config.numStatesToSave > 0)
        outfile << "        <states>" << endl;

    for (int i = 0; i < config.numStatesToSave; i++) {

        int stateID = model->getStatesNameId(config.statesToSave[i].name.c_str(),
                                             config.statesToSave[i].component.c_str());

        if (stateID < 0) {
            cout << endl << "---------------------------------------------------------------" << endl;
            cout << endl << "      ERROR: State not found: " << config.statesToSave[i].name
                 << " (" << config.statesToSave[i].component << ")" << endl;
            cout << endl << "      Configuration file: " << config.fileName << endl;
            cout << endl << "---------------------------------------------------------------" << endl << endl;
            exit(-1);

        }

        outfile << "            <state>" << endl;
        outfile << "                <name>" << config.statesToSave[i].name
                << "</name>" << endl;
        outfile << "                <component>"
                << config.statesToSave[i].component << "</component>" << endl;
        outfile << "            </state>" << endl;
    }

    if (config.numStatesToSave > 0)
        outfile << "        </states>" << endl;

    if (config.numRatesToSave > 0)
        outfile << "        <rates>" << endl;

    for (int i = 0; i < config.numRatesToSave; i++) {

        int rateID = model->getRatesNameId(config.ratesToSave[i].name.c_str(),
                                           config.ratesToSave[i].component.c_str());

        if (rateID < 0) {
            cout << endl << "---------------------------------------------------------------" << endl;
            cout << endl << "      ERROR: Rate not found: " << config.ratesToSave[i].name
                 << " (" << config.ratesToSave[i].component << ")" << endl;
            cout << endl << "      Configuration file: " << config.fileName << endl;
            cout << endl << "---------------------------------------------------------------" << endl << endl;
            exit(-1);

        }

        outfile << "            <rate>" << endl;
        outfile << "                <name>" << config.ratesToSave[i].name
                << "</name>" << endl;
        outfile << "                <component>"
                << config.ratesToSave[i].component << "</component>" << endl;
        outfile << "            </rate>" << endl;
    }

    if (config.numRatesToSave > 0)
        outfile << "        </rates>" << endl;

    if (config.numAlgebraicsToSave > 0)
        outfile << "        <algebraics>" << endl;

    for (int i = 0; i < config.numAlgebraicsToSave; i++) {

        int algebraicID = model->getAlgebraicNameId(config.algebraicsToSave[i].name.c_str(),
                                           config.algebraicsToSave[i].component.c_str());

        if (algebraicID < 0) {
            cout << endl << "---------------------------------------------------------------" << endl;
            cout << endl << "      ERROR: Algebraic not found: " << config.algebraicsToSave[i].name
                 << " (" << config.algebraicsToSave[i].component << ")" << endl;
            cout << endl << "      Configuration file: " << config.fileName << endl;
            cout << endl << "---------------------------------------------------------------" << endl << endl;
            exit(-1);

        }

        outfile << "            <algebraic>" << endl;
        outfile << "                <name>" << config.algebraicsToSave[i].name
                << "</name>" << endl;
        outfile << "                <component>"
                << config.algebraicsToSave[i].component << "</component>" << endl;
        outfile << "            </algebraic>" << endl;
    }

    if (config.numAlgebraicsToSave > 0)
        outfile << "        </algebraics>" << endl;

    outfile << "    </output>" << endl;
    outfile << "</simulation>" << endl;

    outfile.close();

}


void createCondorFiles(GlobalConfig globalConfig) {

    for (int i = 0; i < globalConfig.numProtocols; i++)
        createCondorSubmitFile(globalConfig.path, globalConfig.prefix, globalConfig.protocolNames[i],
                               globalConfig.numCombinations);

}

void createCondorScript(GlobalConfig globalConfig) {

    string condorScriptFileName = globalConfig.path + "/" + globalConfig.prefix + ".sh";
    ofstream condorScript(condorScriptFileName.c_str());
    for (int i = 0; i < globalConfig.numProtocols; i++) {
        condorScript << "echo \"" << i + 1 << "/" << globalConfig.numProtocols << ". " << globalConfig.protocolNames[i]
                     << "\"" << endl;
        condorScript << "condor_submit " << globalConfig.prefix << "/" << globalConfig.prefix << "-"
                     << globalConfig.protocolNames[i] << ".sub" << endl;
        condorScript << "echo \"\"" << endl;
    }
    condorScript.close();

}

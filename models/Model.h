//
// Created by Jesús Carro Fernández on 2/1/20.
//
#pragma once

#ifndef DENIS_MYOCYTE_MODEL_H_
#define DENIS_MYOCYTE_MODEL_H_

//#include "../denis-myocyte-lib/structs/ConstantToChange.h"
#include <iostream>
#include <vector>

class Model {
protected:
    int algebraicsLength;
    int statesLength;
    int constantsLength;
    const char *VoI;
    const char **constantsNames;
    const char **statesNames;
    const char **ratesNames;
    const char **algebraicNames;
    double *constants;
    double *initStates;

    // Abstract private functions
    virtual void setVectorsLength() = 0;

    virtual void setNames() = 0;

    virtual void initConstsAndStates() = 0;

    void initVectors();

    int getNameId(const char *variable, const char *component, const char **names, int namesLength);

public:
    virtual ~Model() = 0;

    // Abstract public functions
    virtual void computeRates(double VoI, double *rates, double *states, double *algebraic) = 0;

    virtual void computeAlgebraics(double VoI, double *rates, double *states, double *algebraic) = 0;

    virtual void setDependentConsts() = 0;

    int getConstantNameId(const char *variable, const char *component);

    int getAlgebraicNameId(const char *variable, const char *component);

    int getStatesNameId(const char *variable, const char *component);

    int getRatesNameId(const char *variable, const char *component);

    const char **getConstantNames() { return constantsNames; }

    const char **getAlgebraicNames() { return algebraicNames; }

    const char **getStateNames() { return statesNames; }

    const char **getRateNames() { return ratesNames; }

    const char *getVoIName() { return VoI; }

    const char *getConstantName(int id) { return constantsNames[id]; }

    const char *getAlgebraicName(int id) { return algebraicNames[id]; }

    const char *getStateName(int id) { return statesNames[id]; }

    const char *getRateName(int id) { return ratesNames[id]; }

    int getConstantsLength() { return constantsLength; }

    int getAlgebraicsLength() { return algebraicsLength; }

    int getStatesLength() { return statesLength; }

    int getRatesLength() { return statesLength; }

    double *getInitStates() {
        double *states = new double[statesLength];
        for(int i=0;i<statesLength;i++)
            states[i]=initStates[i];
        return states;
    }

    double *getConstants() { return constants; }

//    std::vector<ConstantToChange> modifyConstants(std::vector<ConstantToChange> constantsToModify);
};

#endif /* DENIS_MYOCYTE_MODEL_H_ */


void createCondorSubmitFile(string filePath, string simulationName, string protocol, int numSimulations){ 

    string fileName = filePath + "/" + simulationName + "/" + simulationName + "-" + protocol + ".sub"; 
    ofstream condorFile(fileName.c_str());

    condorFile << "Universe = vanilla" << endl;
    condorFile << "Executable = /home/bio/jcarro/DENIS-apps/bin/denis-base-app" << endl << endl;

    condorFile << "PATH_FILES = " << filePath << endl;
    condorFile << "MAIN_NAME = " << simulationName << endl;
    condorFile << "NUM_SIM = " << numSimulations << endl << endl;

    condorFile << "Getenv = TRUE" << endl << endl;

    condorFile << "priority = -$(Process)" << endl;
    condorFile << "+ShortJob = TRUE" << endl << endl;

    condorFile << "PROTOCOL = " << protocol << endl;
    condorFile << "output  =  $(PATH_FILES)/$(MAIN_NAME)/$(PROTOCOL)/Out/$(MAIN_NAME)-$(PROTOCOL)-conf_$(Process).out" << endl;
    condorFile << "error   =  $(PATH_FILES)/$(MAIN_NAME)/$(PROTOCOL)/Out/$(MAIN_NAME)-$(PROTOCOL)-conf_$(Process).error" << endl;
    condorFile << "log     =  $(PATH_FILES)/$(MAIN_NAME)/$(PROTOCOL)/Out/$(MAIN_NAME)-$(PROTOCOL)-conf_$(Process).log" << endl << endl;

    condorFile << "request_memory = 2000" << endl << endl;

    condorFile << "arguments =  $(PATH_FILES)/$(MAIN_NAME)/$(PROTOCOL)/$(MAIN_NAME)-$(PROTOCOL)-conf_$(Process).xml "; 
    condorFile << "$(PATH_FILES)/$(MAIN_NAME)/$(PROTOCOL)/$(MAIN_NAME)-$(PROTOCOL)-conf_$(Process).out ";
    condorFile << "$(PATH_FILES)/$(MAIN_NAME)/$(PROTOCOL)/$(MAIN_NAME)-$(PROTOCOL)-conf_$(Process).markers" << endl;
    condorFile << "Queue $(NUM_SIM)" << endl;

    condorFile.close();

}

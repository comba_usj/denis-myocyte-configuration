#include <iostream>
#include <fstream>
#include <string.h>
#include "model_modif_constant.h"

using namespace std;

class GlobalConfig {

public:
    string prefix;
    string path;

    int numProtocols;
    string *protocolNames;
    string *modelNames;
    string *vNames;
    string *vComponents;
    double *time;

    int numCombinations;
    string *solver;
    double *dt;
    int *freqOut;
    double *startOut;
    double *startPost;


    ModelModifConstant *globalConstantsToChange;
    int numGlobalConstantsToChange;
    ModelElement *statesToSave;
    int numStatesToSave;
    ModelElement *ratesToSave;
    int numRatesToSave;
    ModelElement *algebraicsToSave;
    int numAlgebraicsToSave;

    ModelModifConstant **protocolConstantsToChange;
    int *numProtocolConstantsToChange;
    ModelElement **protocolStatesToSave;
    int *numProtocolStatesToSave;
    ModelElement **protocolRatesToSave;
    int *numProtocolRatesToSave;
    ModelElement **protocolAlgebraicsToSave;
    int *numProtocolAlgebraicsToSave;


    GlobalConfig(const char *globalConfigFile);
};

GlobalConfig::GlobalConfig(const char *globalConfigFile) {

    ifstream input(globalConfigFile);
    input >> path;
    input >> prefix;
    input >> numCombinations;

    cout << "File: " << globalConfigFile << endl;
    cout << "Path: " << path << endl;
    cout << "Prefix: " << prefix << endl;
    cout << "Number of combinations: " << numCombinations << endl;
    input >> numGlobalConstantsToChange;
    cout << endl << "Number of global constantsToChange to change: "
         << numGlobalConstantsToChange << endl;

    globalConstantsToChange = new ModelModifConstant[numGlobalConstantsToChange];

    string name, component;
    double initValue, endValue;
    int typeInteger;
    ModificationType type;

    for (int i = 0; i < numGlobalConstantsToChange; i++) {
        input >> name;
        input >> component;
        input >> initValue;
        input >> endValue;
        input >> typeInteger;
        type = (ModificationType) typeInteger;

        globalConstantsToChange[i] = ModelModifConstant(name, component, initValue,
                                                        endValue, type);
        if (type) {
            cout << "    - " << name << " (" << component << "): "
                 << initValue << "dB - " << endValue << "dB"
                 << endl;
        } else {
            cout << "    - " << name << " (" << component << "): " << initValue
                 << " - " << endValue << endl;
        }
    }

    input >> numStatesToSave;
    cout << endl << "Number of states to save: " << numStatesToSave
         << endl;

    statesToSave = new ModelElement[numStatesToSave];

    for (int i = 0; i < numStatesToSave; i++) {
        input >> name;
        input >> component;
        statesToSave[i] = ModelElement(name, component);
        cout << "    - " << name << " (" << component << ")" << endl;

    }

    input >> numRatesToSave;
    cout << endl << "Number of rates to save: " << numRatesToSave << endl;

    ratesToSave = new ModelElement[numRatesToSave];

    for (int i = 0; i < numRatesToSave; i++) {
        input >> name;
        name = "d/dt " + name;
        input >> component;
        ratesToSave[i] = ModelElement(name, component);
        cout << "    - " << name << " (" << component << ")" << endl;

    }

    input >> numAlgebraicsToSave;
    cout << endl << "Number of algebraics to save: " << numAlgebraicsToSave << endl;

    algebraicsToSave = new ModelElement[numAlgebraicsToSave];

    for (int i = 0; i < numAlgebraicsToSave; i++) {
        input >> name;
        input >> component;
        algebraicsToSave[i] = ModelElement(name, component);
        cout << "    - " << name << " (" << component << ")" << endl;

    }

    input >> numProtocols;
    cout << endl << "Number of protocols: " << numProtocols << endl;

    protocolNames = new string[numProtocols];
    modelNames = new string[numProtocols];
    vNames = new string[numProtocols];
    vComponents = new string[numProtocols];
    time = new double[numProtocols];
    solver = new string[numProtocols];
    dt = new double[numProtocols];
    startOut = new double[numProtocols];
    freqOut = new int[numProtocols];
    startPost = new double[numProtocols];

    protocolConstantsToChange = new ModelModifConstant *[numProtocols];
    numProtocolConstantsToChange = new int[numProtocols];
    protocolStatesToSave = new ModelElement *[numProtocols];
    numProtocolStatesToSave = new int[numProtocols];
    protocolRatesToSave = new ModelElement *[numProtocols];
    numProtocolRatesToSave = new int[numProtocols];
    protocolAlgebraicsToSave = new ModelElement *[numProtocols];
    numProtocolAlgebraicsToSave = new int[numProtocols];

    for (int i = 0; i < numProtocols; i++) {
        input >> protocolNames[i];
        input >> modelNames[i];
        input >> vNames[i];
        input >> vComponents[i];

        input >> time[i];
        input >> solver[i];
        input >> dt[i];

        double timeToSave;
        input >> timeToSave;
        startOut[i] = time[i] - timeToSave;
        input >> freqOut[i];
        double timeToPost;
        input >> timeToPost;
        startPost[i] = time[i] - timeToPost;

        cout << endl << "Protocol " << i + 1 << ": " << protocolNames[i]
             << endl;
        cout << "    - Model: " << modelNames[i] << endl;
        cout << "    - Membrane potential: " << vNames[i] << "("
             << vComponents[i] << ")" << endl;
        cout << "    - Simulation time: " << time[i] << endl;
        cout << "    - Solver: " << solver[i] << endl;
        cout << "    - Time step: " << dt[i] << endl;
        cout << "    - Output start: " << startOut[i] << endl;
        cout << "    - Output frequency: " << freqOut[i] << endl;
        cout << "    - Post start: " << startPost[i] << endl;

        input >> numProtocolConstantsToChange[i];
        protocolConstantsToChange[i] = new ModelModifConstant[numProtocolConstantsToChange[i]];

        cout << "    - Number of constantsToChange to change: "
             << numProtocolConstantsToChange[i] << endl;

        for (int j = 0; j < numProtocolConstantsToChange[i]; j++) {
            input >> name;
            input >> component;
            input >> initValue;
            input >> typeInteger;
            type = (ModificationType) typeInteger;

            protocolConstantsToChange[i][j] = ModelModifConstant(name, component,
                                                                 initValue, type);
            cout << "        * " << name << " (" << component << "): " << initValue;

            if (type) {
                cout << "dB";
            }

            cout << endl;
        }

        input >> numProtocolStatesToSave[i];
        protocolStatesToSave[i] =
                new ModelElement[numProtocolStatesToSave[i]];

        cout << "    - Number of states to save: "
             << numProtocolStatesToSave[i] << endl;

        for (int j = 0; j < numProtocolStatesToSave[i]; j++) {
            input >> name;
            input >> component;
            protocolStatesToSave[i][j] = ModelElement(name, component);

            cout << "        * " << name << " (" << component << ")" << endl;

        }

        input >> numProtocolRatesToSave[i];
        protocolRatesToSave[i] =
                new ModelElement[numProtocolRatesToSave[i]];

        cout << "    - Number of rates to save: "
             << numProtocolRatesToSave[i] << endl;

        for (int j = 0; j < numProtocolRatesToSave[i]; j++) {
            input >> name;
            name = "d/dt " + name;
            input >> component;
            protocolRatesToSave[i][j] = ModelElement(name, component);

            cout << "        * " << name << " (" << component << ")" << endl;

        }

        input >> numProtocolAlgebraicsToSave[i];
        protocolAlgebraicsToSave[i] =
                new ModelElement[numProtocolAlgebraicsToSave[i]];

        cout << "    - Number of algebraics to save: "
             << numProtocolAlgebraicsToSave[i] << endl;

        for (int j = 0; j < numProtocolAlgebraicsToSave[i]; j++) {
            input >> name;
            input >> component;
            protocolAlgebraicsToSave[i][j] = ModelElement(name, component);

            cout << "        * " << name << " (" << component << ")" << endl;

        }

    }

    cout << endl;

}

using namespace std;

class Config {

public:
    string fileName;
    string modelName;
    double time;

    string solver;
    double dt;

    int freqOut;
    double startOut;

    double startPost;

    string vName;
    string vComponent;

    ModelConstant *constantsToChange;
    int numConstantsToChange;
    ModelElement *statesToSave;
    int numStatesToSave;
    ModelElement *ratesToSave;
    int numRatesToSave;
    ModelElement *algebraicsToSave;
    int numAlgebraicsToSave;

    Config() {}

    Config(string fileName, string modelName, double time, string solver,
           double dt, int freqOut, double startOut, double startPost,
           string vName, string vComponent, ModelConstant *constantsToChange,
           int numConstantsToChange, ModelElement *statesToSave,
           int numStatesToSave, ModelElement *ratesToSave,
           int numRatesToSave, ModelElement *algebraicsToSave,
           int numAlgebraicsToSave) {
        this->fileName = string(fileName);
        this->modelName = string(modelName);
        this->time = time;
        this->solver = solver;
        this->dt = dt;
        this->freqOut = freqOut;
        this->startOut = startOut;
        this->startPost = startPost;
        this->vName = string(vName);
        this->vComponent = string(vComponent);
        this->constantsToChange = constantsToChange;
        this->numConstantsToChange = numConstantsToChange;
        this->statesToSave = statesToSave;
        this->numStatesToSave = numStatesToSave;
        this->ratesToSave = ratesToSave;
        this->numRatesToSave = numRatesToSave;
        this->algebraicsToSave = algebraicsToSave;
        this->numAlgebraicsToSave = numAlgebraicsToSave;

    }

};
